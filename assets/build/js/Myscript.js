//table-detail-client
var table = $('#example').DataTable({
  'paging'      : true,
  'lengthChange': true,
  'searching'   : true,
  'ordering'    : true,
  'info'        : true,
  'autoWidth'   : true,
  'language': {
    "zeroRecords": "Sorry, no clients to display", //changes words used
    "lengthMenu": "Show _MENU_ clients per page", //changes words used
    "info": "&raquo; Showing _START_ to _END_ of _TOTAL_ clients", //changes words used
    "search": "", //changes words used originally - Search clients:
    "searchPlaceholder": "Search Client",
    "infoFiltered": "(filtered from _MAX_ total clients)"
  }
});