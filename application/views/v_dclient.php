<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detail Client
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Detail Client</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example" class="table table-bordered table-striped">
            <ul class="list-group">
              <a href="<?php echo base_url('dclient') ?>" class="btn btn-default btn-sm">All</a>
              <a href="<?php echo base_url('dclient/subscribe') ?>" class="btn btn-success btn-sm">Subscribe</a>
              <a href="<?php echo base_url('dclient/unsubscribe') ?>" class="btn btn-danger btn-sm">Unsubscribe</a>
              <a href="<?php echo base_url('dclient/free') ?>" class="btn btn-warning btn-sm">Free</a>
              <a href="<?php echo base_url('dclient/trial') ?>" class="btn btn-info btn-sm">Trial</a>
            </ul>
            <thead>
            <tr>
              <th>No</th>
              <th>User ID</th>
              <th>SC</th>
              <th>Nama Client</th>
              <th>Contact Person</th>
              <th>Tlp/HP</th>
              <th>Alamat</th>
              <th>Status</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach($data as $u){ ?>
                <tr role="row" class="odd">
                  <td class="sorting_1"><?php echo $no++ ?></td>
                  <td><a href="edit_client/<?php echo $u->user_id ?>"><?php echo $u->user_id ?></a></td>
                  <td><?php echo $u->scver ?></td>
                  <td><?php echo $u->uname ?></td>
                  <td><?php echo $u->contact_person ?></td>
                  <td><?php echo $u->tlp1 ?> , <?php echo $u->tlp2 ?> , <?php echo $u->tlp3 ?></td>
                  <td><?php echo $u->address1 ?> , <?php echo $u->address2 ?></td>
                  <td><?php echo $u->sctyp ?></td>  
                </tr>
              <?php } ?>
            </tbody>
            <tfoot>
            <tr>
              <th>No</th>
              <th>User ID</th>
              <th>SC</th>
              <th>Nama Client</th>
              <th>Contact Person</th>
              <th>Tlp/HP</th>
              <th>Alamat</th>
              <th>Status</th>
            </tr>
            </tfoot>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->

