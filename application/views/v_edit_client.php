  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Detail Client
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Edit Detail Client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputUserid" class="col-sm-2 control-label">User ID</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputUserid" placeholder="User ID">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputUname" class="col-sm-2 control-label">Client</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputUname" placeholder="Nama Client">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputHp1" class="col-sm-2 control-label">CP</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputHp1" placeholder="HP 1">
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputAn1" placeholder="Atas nama 1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputHp1" class="col-sm-2 control-label"></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputHp1" placeholder="HP 2">
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputAn2" placeholder="Atas nama 2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputFax" class="col-sm-2 control-label">FAX</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputFax" placeholder="Fax">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAdd1" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputAdd1" placeholder="Address 1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAdd2" class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputAdd2" placeholder="Address 2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCity" class="col-sm-2 control-label">City</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputCity" placeholder="City">
                  </div>
                  <label for="inputState" class="col-sm-2 control-label">State</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputState" placeholder="State">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCountry" class="col-sm-2 control-label">Country</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputCountry" placeholder="Country">
                  </div>
                  <label for="inputZip" class="col-sm-2 control-label">ZIP</label>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="inputZip" placeholder="ZIP">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputScdate" class="col-sm-2 control-label">SC Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="inputScdate" placeholder="--Date--">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputUserid" class="col-sm-2 control-label">Proci</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputUserid" placeholder="Processor">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputOsv" class="col-sm-2 control-label">OS Versi</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputOsv" placeholder="OS Version">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
        </div>
        <!--col (right) -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputLocalip" class="col-sm-2 control-label">Local IP</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputLocalip" placeholder="Local IP">
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputTcpport" placeholder="TCP Port">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputMcsip" class="col-sm-2 control-label">MCS IP</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputMcsip" placeholder="MCS Group IP">
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="inputMcsport" placeholder="MCS Port">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputScver" class="col-sm-2 control-label">SC Versi</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputScver" placeholder="SC Version">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputFax" class="col-sm-2 control-label">FAX</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputFax" placeholder="Fax">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputFax" class="col-sm-2 control-label">FAX</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputFax" placeholder="Fax">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAdd1" class="col-sm-2 control-label">Address</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputAdd1" placeholder="Address 1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAdd2" class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputAdd2" placeholder="Address 2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCity" class="col-sm-2 control-label">City</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputCity" placeholder="City">
                  </div>
                  <label for="inputState" class="col-sm-2 control-label">State</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputState" placeholder="State">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCountry" class="col-sm-2 control-label">Country</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="inputCountry" placeholder="Country">
                  </div>
                  <label for="inputZip" class="col-sm-2 control-label">ZIP</label>
                  <div class="col-sm-4">
                    <input type="number" class="form-control" id="inputZip" placeholder="ZIP">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputScdate" class="col-sm-2 control-label">SC Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="inputScdate" placeholder="--Date--">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputUserid" class="col-sm-2 control-label">Proci</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputUserid" placeholder="Processor">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputOsv" class="col-sm-2 control-label">OS Versi</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputOsv" placeholder="OS Version">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url ('dclient') ?>" class="btn btn-danger" role="button">Batal</a>
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>

          </div>
        </div>
        <!--col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->