<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dclient extends CI_Controller {

	public function index(){

		$data['data'] = $this->m_dclient->tampil_data()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_dclient',$data);
		$this->load->view('templates/footer');
	}

	public function subscribe(){

		$data['data'] = $this->m_dclient->tampil_data_subscribe()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_dclient',$data);
		$this->load->view('templates/footer');
	}
	public function unsubscribe(){

		$data['data'] = $this->m_dclient->tampil_data_unsubscribe()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_dclient',$data);
		$this->load->view('templates/footer');
	}
	public function free(){

		$data['data'] = $this->m_dclient->tampil_data_free()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_dclient',$data);
		$this->load->view('templates/footer');
	}
	public function trial(){

		$data['data'] = $this->m_dclient->tampil_data_trial()->result();
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_dclient',$data);
		$this->load->view('templates/footer');
	}

}
