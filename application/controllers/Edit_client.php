<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_client extends CI_Controller {

	public function index(){
		$this->load->view('templates/header');
		$this->load->view('templates/sidebar');
		$this->load->view('v_edit_client');
		$this->load->view('templates/footer');
	}
}
