<?php 

class m_dclient extends CI_Model{
	public function tampil_data(){
		return $this->db->get('data_client');
	}

	public function tampil_data_subscribe(){

		$subscribe = array ('Subscribe');
		$this->db->where_in('sctyp',$subscribe);
		return $this->db->get('data_client');
	}
	public function tampil_data_unsubscribe(){

		$unsubscribe = array ('Unsubscribe');
		$this->db->where_in('sctyp',$unsubscribe);
		return $this->db->get('data_client');
	}
	public function tampil_data_free(){

		$free = array ('Free');
		$this->db->where_in('sctyp',$free);
		return $this->db->get('data_client');
	}
	public function tampil_data_trial(){

		$trial = array ('Trial');
		$this->db->where_in('sctyp',$trial);
		return $this->db->get('data_client');
	}
}